#include <iostream>
#include "stopwatch.h"
#include "tqdm.h"
#include "profile.h"

int main()
{
	const unsigned long long ITERATIONS = 10000000U;
	Stopwatch sw;
	double durWith;
	double durWithout;

	// time a loop of code with the PROFILE() macro in it
	sw.start();
	for (unsigned long long i=0; i<ITERATIONS; i++)
	{
		PROFILE();
		unsigned long long x = i;
		x *= 2;
	}
	sw.stop();

	durWith = sw.elapsed();
	sw.reset();

	// time same code without the PROFILE() macro in it
	sw.start();
	for (unsigned long long i=0; i<ITERATIONS; i++)
	{
		unsigned long long x = i;
		x *= 2;
	}
	sw.stop();

	durWithout = sw.elapsed();
	double avgOverhead = (durWith - durWithout)/static_cast<double>(ITERATIONS);

	std::cout << "duration with:              " << durWith     << std::endl;
	std::cout << "duration without:           " << durWithout  << std::endl;
	std::cout << "avg overhead per iteration: " << avgOverhead << std::endl;

	return 0;
}