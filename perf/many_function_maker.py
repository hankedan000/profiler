n_functions = 1000

def make_source(filename,num_functions):
	with open(filename,'w') as file:
		file.write("#include <unistd.h>\n")
		file.write("#include <stdlib.h>\n")
		file.write("#include <stdio.h>\n\n")

		FIXED_SLEEP = 100
		for i in range(n_functions):
			file.write("int function%d(int i){usleep(rand()%%%d + %d); return i*%d;}\n" % (i,FIXED_SLEEP,FIXED_SLEEP+i,i))

		file.write("\n")
		file.write("int main()\n{\n")
		file.write("\twhile (true)\n\t{\n")
		file.write("\t\tprintf(\"looping!\\n\");\n")
		for i in range(n_functions):
			file.write("\t\tfunction%d(%d);\n" % (i,i))
		file.write("\t}\n")
		file.write("\treturn 0;\n}")

make_source("many_functions.cpp",1000)