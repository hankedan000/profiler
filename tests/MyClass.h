#ifndef MY_CLASS_H_
#define MY_CLASS_H_

namespace tests
{

	class MyClass
	{
	public:
		MyClass();

		void funcA();

		bool funcB();
	};

}

#endif