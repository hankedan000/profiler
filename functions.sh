#!/bin/bash

# returns a list of all C source files
get_all_c_files() {
	dir=$1
	find $dir -name "*.c" -o -name "*.cpp" -o -name "*.cxx"
}

# function used to strip all profiling from a C/C++ source file
rm_profiling() {
	if [[ -f $1 ]]
	then
		file=$1
		verbose=1
		if [[ $# -gt 1 ]]; then
			verbose=$2
		fi
		if [[ $verbose -eq 1 ]]; then
			echo "Removing profiling from $file"
		fi

		spatch $PROFILE_COCCI/add_profile.cocci $SPATCH_OPTS --reverse $file 2> /dev/null
		spatch $PROFILE_COCCI/rm_include.cocci $SPATCH_OPTS $file 2> /dev/null
	elif [[ -d "$1" ]]
	then
		# adding profiling to a directory of source file
		dir=$1

		# get a list of all the C/C++ source files in the directory
		c_files=$(get_all_c_files $dir)

		# add profiling to each one
		for file in $c_files; do
			rm_profiling $file
		done
	else
		echo "Nothing to remove profiling from!"
	fi
}

# function used to add profiling to a C/C++ source file
add_profiling() {
	if [[ -f $1 ]]
	then
		# adding profiling to a single source file
		file=$1
		verbose=1
		if [[ $# -gt 1 ]]; then
			verbose=$2
		fi
		if [[ $verbose -eq 1 ]]; then
			echo "Adding profiling to $file"
		fi

		# start by stripping all existing profiling
		rm_profiling $file 0

		# add profiling back to source file
		spatch $PROFILE_COCCI/add_include.cocci $SPATCH_OPTS $file 2> /dev/null
		spatch $PROFILE_COCCI/add_profile.cocci $SPATCH_OPTS $file 2> /dev/null
	elif [[ -d "$1" ]]
	then
		# adding profiling to a directory of source file
		dir=$1

		# get a list of all the C/C++ source files in the directory
		c_files=$(get_all_c_files $dir)

		# add profiling to each one
		for file in $c_files; do
			add_profiling $file
		done
	else
		echo "Nothing to add profiling to!"
	fi
}
