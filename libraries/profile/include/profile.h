#ifndef PROFILE_PROFILE_H_
#define PROFILE_PROFILE_H_

#include <string>
#include <memory>
#include <tuple>
#include <unordered_map>
#include <map>
#include <iostream>
#include <thread>
#include <mutex>
#include <sstream>
#include <functional>
#include "stopwatch.h"

#include "tcp_client.h"

namespace profile
{

	// base class for any profile streamer
	class Streamer
	{
	public:
		Streamer() {}

		virtual ~Streamer() {}

		virtual bool configure() {return true;}

		virtual bool open() {return true;}

		virtual void close() {}

	};

	// TCP socketed implementation of streamer
	class SocketStreamer : public Streamer
	{
	public:
		SocketStreamer()
		 : shutdown_(false)
		{
		}

		virtual
		bool
		open() override
		{
			// spawn a worker thread for streaming profiler information
			thread_ = std::thread(&SocketStreamer::stream,this);

			return true;
		}

		virtual
		void
		close() override
		{
			std::cout << "[SocketStreamer]: Waiting for thread to join..." << std::endl;
			shutdown_ = true;
			thread_.join();
		}

		void
		stream();

	private:
		std::thread thread_;
		bool shutdown_;
		Client client_;

	};

	class TheAggregator
	{
	public:
		typedef size_t entry_uuid_t;

		struct Entry
		{
			Entry(
				const std::string &file,
				const std::string &func,
				const uint32_t &line)
			 : file_name(file)
			 , fun_name(func)
			 , line_num(line)
			 , sw()
			 , count(0)
			{}
			
			std::string
			toString()
			{
				std::stringstream ss;
				ss << file_name << "|" << fun_name << "|" << line_num << "|" << sw.elapsed() << "|" << count;
				return ss.str();
			}

			// the source file name where profile entry is measuring
			std::string file_name;
			// function name where profile entry is measuring
			std::string fun_name;
			// source line number where profile entry is measuring
			uint32_t line_num;
			// stop watch class used to time entries ellapsed duration
			Stopwatch sw;
			// total execution count for this entry
			size_t count;
		};

	public:
		~TheAggregator()
		{
			streamer_->close();
		}

		static
		TheAggregator &
		inst()
		{
			static TheAggregator instance;
			return instance;
		}

		std::shared_ptr<Entry> &
		getEntry(
			const std::string &file,
			const std::string &func,
			const uint32_t &line,
			entry_uuid_t uuid)
		{
			auto itr = entries_.find(uuid);
			if (itr != entries_.end())
			{
				return itr->second;
			}
			else
			{
				entriesLock_.lock();
				auto new_entry = entries_.insert(std::make_pair(uuid,std::shared_ptr<Entry>(new Entry(file,func,line))));
				entriesLock_.unlock();
				return new_entry.first->second;
			}
		}

		std::shared_ptr<Entry> &
		getEntry(
			entry_uuid_t uuid)
		{
			auto itr = entries_.find(uuid);
			return itr->second;
		}

		std::vector<std::string>
		getEntries()
		{
			const unsigned int MAX_ENTRIES = 10U;
			std::map<double,std::shared_ptr<Entry>> slowestEntries;
			std::vector<std::string> entries;

			entriesLock_.lock();
			for (const auto &entry : entries_)
			{
				slowestEntries.insert({entry.second->sw.elapsed(),entry.second});
				if (slowestEntries.size() > MAX_ENTRIES)
				{
					slowestEntries.erase(slowestEntries.begin());
				}
			}
			for (const auto &entry : slowestEntries)
			{
				entries.push_back(entry.second->toString());
			}
			entriesLock_.unlock();
			return entries;
		}

	private:
		TheAggregator()
		 : streamer_(nullptr)
		{
			streamer_ = std::unique_ptr<SocketStreamer>(new SocketStreamer());

			if (streamer_->configure())
			{
				streamer_->open();
			}
			else
			{
				std::cerr << "[TheAggregator]: Error failed to configure SocketStreamer." << std::endl;
			}
		}

	private:
		std::unique_ptr<Streamer> streamer_;
		std::mutex entriesLock_;
		std::unordered_map<entry_uuid_t,std::shared_ptr<Entry>> entries_;

	};

	class ProfileObj
	{
	public:
		ProfileObj(
			const std::string &file,
			const std::string &func,
			const uint32_t &line,
			size_t uuid)
		 : uuid(uuid)
		{
			std::shared_ptr<TheAggregator::Entry> &entry = TheAggregator::inst().getEntry(file,func,line,uuid);
			entry->count++;
			entry->sw.start();
		}

		~ProfileObj()
		{
			std::shared_ptr<TheAggregator::Entry> &entry = TheAggregator::inst().getEntry(uuid);
			entry->sw.stop();
		}

		size_t uuid;

	};

	inline
	void
	SocketStreamer::stream()
	{
		const unsigned int REFRESH_RATE_HZ = 10U;
		bool connected = client_.Init();

		while (connected && ! shutdown_)
		{
			usleep(1000000/REFRESH_RATE_HZ);

			auto entries = TheAggregator::inst().getEntries();
			client_.Send("#BEGIN");
			for (auto &entry : entries)
			{
				client_.Send(entry);
			}
			client_.Send("#END");
		}

		std::cout << "[SocketStreamer]: thread exited!" << std::endl;
	}

}// namespace profiler

// creates a static variable that contains a globally unique identifier
#define STATIC_FUNC_HASH(HASH_SEED) static size_t __profiler_unique_hash_(std::hash<std::string>{}(__FILE__) ^ (std::hash<size_t>{}(HASH_SEED) << 1))
// creates a profiler object for use inside of a function's cope
#define FUNC_PROFILER_OBJ(HASH_SEED) STATIC_FUNC_HASH(HASH_SEED); profile::ProfileObj __profile_obj(__FILE__,__PRETTY_FUNCTION__,__LINE__,__profiler_unique_hash_)
// short macro used to inject profiling code
#define PROFILE() FUNC_PROFILER_OBJ(__COUNTER__)

#endif
