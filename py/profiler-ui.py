#!/usr/bin/env python3
import socket
import argparse

parser = argparse.ArgumentParser(
    prog='profiler-ui',
    usage='%(prog)s [options]',
    description='Services a socket connection to compute fips vectors')

args = parser.parse_args()

##########################################################

class MsgAssembler():
	def __init__(self):
		self.msgs = []
		self.saw_begin = False

	def add(self,msg):
		group = None
		if msg == "#BEGIN":
			self.saw_begin = True
		elif msg == "#END":
			if self.saw_begin:
				group = self.msgs
			self.msgs = []
			self.saw_begin = False
		else:
			self.msgs.append(msg)
		return group

##########################################################

# bytes size of message length packet
MESSAGE_LENGTH_SIZE = 16

def wait_for_msg(sock,debug=False):
	msg = ""
	# wait for message length from client
	data, addr = sock.recvfrom(MESSAGE_LENGTH_SIZE)
	data_str = data.decode("utf-8")

	msg_length = 0
	try:
		msg_length = int(data_str)
	except:
		pass

	if debug:
		print('Accepting msg of length %d' % msg_length)
		print('Found a client! %s' % (str(addr)))

	if msg_length > 0:
		# wait for reception of message
		data, addr = sock.recvfrom(msg_length)
		msg = data.decode("utf-8")

	return msg

##########################################################

# display profile results in a prettytable
from prettytable import PrettyTable
import os

def clear_terminal():
	os.system('cls' if os.name == 'nt' else 'clear')

def display_table(msgs):
	table = PrettyTable()

	table.field_names = ["file","function","line","duration (s)","iterations"]

	for msg in msgs:
		row_vals = msg.split('|')

		# convert some columns to numbers for table sorting
		if len(row_vals) >= 5:
			row_vals[2] = int(row_vals[2])# line number
			row_vals[3] = float(row_vals[3])# duration
			row_vals[4] = int(row_vals[4])# iterations
			
		table.add_row(row_vals)

	clear_terminal()
	table.sortby = "duration (s)"
	table.align = 'l'
	print(table)

def run_ui_server(host,port=5001):
	assembler = MsgAssembler()
	s = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
	try:
		s.bind((host,port))
	except:
		s.bind(('localhost',port))

	print('Listening...')

	try:
		while True:
			msg = wait_for_msg(s)
			msgs = assembler.add(msg)
			if msgs:
				# message group received!
				display_table(msgs)

	except KeyboardInterrupt:
		print('Shutting down...')

	s.close()
	print('Profiler UI server done!')

##########################################################

if __name__ == "__main__":
	run_ui_server('127.0.0.1',5001)
