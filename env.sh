#!/bin/sh
export PROFILE_ROOT="$(pwd)"
export PROFILE_INCLUDE=$PROFILE_ROOT/include
export PROFILE_PATH=$PROFILE_ROOT/lib
export PROFILE_COCCI=$PROFILE_ROOT/cocci
export CFLAGS="-Wall -Werror"

export SPATCH_OPTS="-in_place --keep-comments --no-show-diff"

# source the profiler helper functions
source functions.sh
