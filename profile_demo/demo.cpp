#include <iostream>
#include <signal.h>
#include "tqdm.h"
#include "profile_me.h"
#include "profile.h"

bool terminate = false;

void sigint_handler(int /*signum*/)
{
	PROFILE();
	// terminate program
	terminate = true;
}

void slower()
{
	PROFILE();
	slow();
	usleep(100);
}

void slowest()
{
	PROFILE();
	slower();
	usleep(1000);
}

int main()
{
	PROFILE();
	// Register signal and signal handler
	signal(SIGINT, sigint_handler);

	std::cout << "==========================================================\n";
	std::cout << "=                     INPUT ARGUMENTS                    =\n";
	std::cout << "==========================================================\n";
	std::cout << "==========================================================\n";

	tqdm bar;
	const unsigned int MAX_COUNT = 1000;
	std::vector<int> list;
	for ( unsigned int i=0; !terminate && i<MAX_COUNT; i++)
	{
		bar.progress(i,MAX_COUNT);
		slowest();
	}

	return 0;
}
